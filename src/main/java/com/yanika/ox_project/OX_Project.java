/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.yanika.ox_project;

import java.util.Scanner;

/**
 *
 * @author ASUS
 */
public class OX_Project {

    public static void main(String[] args) {
        char[][] board = new char[3][3];

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = '-';
            }
        }


        Scanner kb = new Scanner(System.in);

        boolean player1 = true;
        boolean gameEnded = false;
        
        
         String p1 = "O" ;
        String p2 = "X" ;
        
        while (!gameEnded) {
            
            PrintBoard(board);
        
 if (player1) {
                System.out.println(p1 + "'s Turn (O):");
            } else {
                System.out.println(p2 + "'s Turn (X):");
            }
 
        char c = '-';
        if (player1) {
            c = 'O';
        } else {
            c = 'X';
        }
        
        
        int row = 0;
            int col = 0;

            while (true) {

                System.out.print("Please input row, col: : ");
                row = kb.nextInt();
                col = kb.nextInt();

                if (row < 0 || col < 0 || row > 2 || col > 2) {
                    System.out.println("Please Input number (0-2)!!!");
                } else if (board[row][col] != '-') {
                    System.out.println("Please Input number (0-2)!!!");
                } else {
                    break;
                }

            }
              board[row][col] = c;
              
                if (playerHasWon(board) == 'O') {
                System.out.println(p1 + " has won!");
                gameEnded = true;
            } else if (playerHasWon(board) == 'X') {
                System.out.println(p2 + " has won!");
                gameEnded = true;
            } else {
                if (Draw(board)) {
                    System.out.println("It's a tie!");
                    gameEnded = true;
                } else {
                    player1 = !player1;
                }
            }


    }
    }
    
    public static char playerHasWon(char[][] board) {
        for (int i = 0; i < 3; i++) {
            if (board[i][0] == board[i][1] && board[i][1] == board[i][2] && board[i][0] != '-') {
                return board[i][0];
            }
        }
        for (int j = 0; j < 3; j++) {
            if (board[0][j] == board[1][j] && board[1][j] == board[2][j] && board[0][j] != '-') {
                return board[0][j];
            }
        }
        if (board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[0][0] != '-') {
            return board[0][0];
        }
        if (board[2][0] == board[1][1] && board[1][1] == board[0][2] && board[2][0] != '-') {
            return board[2][0];
        }
        return ' ';
    }
    
        public static boolean Draw(char[][] board) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    public static void PrintBoard(char[][] board) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j]);
            }
            System.out.println();
        }
    }

}
